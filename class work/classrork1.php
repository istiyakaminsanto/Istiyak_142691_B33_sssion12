<!DOCTYPE html>
<html>
<head>
    <style>

    </style>
</head>
<body>

<h2>CLASS WORK 1 - HTML/CSS</h2>
<p>Create one index.html file to output as follows : (hints: html table and button)</p>
<table border="2"">
    <tr>
        <th>Serial</th>
        <th >ID</th>
        <th >Book Title</th>
        <th >Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td>5</td>
        <td>PHP - BOOK TITLE #1</td>
        <td>
            <input type="submit" value="View">
            <input type="submit" value="Edit">
            <input type="submit" value="Delete">
            <input type="submit" value="Trash">
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>6</td>
        <td>PHP - BOOK TITLE #2</td>
        <td>
            <input type="submit" value="View">
            <input type="submit" value="Edit">
            <input type="submit" value="Delete">
            <input type="submit" value="Trash">
        </td>
    </tr>
    <tr>
        <td>3</td>
        <td>7</td>
        <td>PHP - BOOK TITLE #3</td>
        <td>
            <input type="submit" value="View">
            <input type="submit" value="Edit">
            <input type="submit" value="Delete">
            <input type="submit" value="Trash">
        </td>
    </tr>
    <tr >
        <td colspan="5">PAGE :<
            <a href="#">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">7</a>
            >
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <input type="submit" value="Add New Book Title">
            <input type="submit" value="View Trash Item">
            <input type="submit" value="Download As PDF">
            <input type="submit" value="Download as EXEL File">
        </td>
    </tr>
</table>

</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <style>
        .square-box{
            position: relative;
            width: 50%;
            overflow: hidden;
            background: darkgreen;
        }
        .square-box:before{
            content: "";
            display: block;
            padding-top: 100%;
        }
        .square-content{
            position:  absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            color: white;
        }
        .square-content div {
            display: inline-block;
            width: 100%;
            height: 100%;
        }
        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
            color: white
        }
    </style>
</head>
<body>
<div class='square-box'>
    <div class='square-content'><div><span>Aspect ratio 1:1</span></div>
        <div class='square-content'><div><span>Aspect ratio 1:1</span></div>
</div>
</body>
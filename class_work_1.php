<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>CLASS WORK 1</title>
            <style>

            </style>
            <link rel="stylesheet" href="assetc/css/bootstrap.min.css">
        </head>
        <body>
        <div class="container-fluid">
            <div class="table-responsive">
                <table border="2" class="table table-bordered">
                <tr>
                    <td>Serial</td>
                    <td>ID</td>
                    <td>Book Title</td>
                    <td>Action</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>5</td>
                    <td>PHP BOOK TITLE #1</td>
                    <td><input class="btn btn-info" type="button" value="View">
                        <input class="btn btn-primary" type="button" value="Edit">
                        <input class="btn btn-danger" type="button" value="Delete">
                        <input class="btn btn-warning" type="button" value="Trash">
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>6</td>
                    <td>PHP BOOK TITLE #2</td>
                    <td>
                        <input class="btn btn-info" type="button" value="View">
                        <input class="btn btn-primary" type="button" value="Edit">
                        <input class="btn btn-danger" type="button" value="Delete">
                        <input class="btn btn-warning" type="button" value="Trash">
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>7</td>
                    <td>PHP BOOK TITLE #3</td>
                    <td>
                        <input class="btn btn-info" type="button" value="View">
                        <input class="btn btn-primary" type="button" value="Edit">
                        <input class="btn btn-danger" type="button" value="Delete">
                        <input class="btn btn-warning" type="button" value="Trash">
                    </td>
                </tr>

            </table>
                </div>
        </div>


        </body>

    </html>